## Multi-social media

https://github.com/qeeqbox/social-analyzer  
https://github.com/soxoj/marple  
https://github.com/soxoj/socid-extractor  
https://github.com/soxoj/maigret

## Facebook 

https://lookup-id.com/facebooksearch.html  
https://github.com/harismuneer/Ultimate-Facebook-Scraper

## Instagram 

https://github.com/althonos/InstaLooter  
https://github.com/Datalux/Osintgram  

## Twitter

https://github.com/twintproject/twint
https://inosocial.com/blog/how-to-see-deleted-tweets/

## YouTube

https://github.com/mattwright324/youtube-metadata  
https://github.com/nlitsme/youtube_tool

## LinkedIn 

https://github.com/m8r0wn/CrossLinked
