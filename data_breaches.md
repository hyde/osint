## Telegram 

Here is a list of Telegram groups where data breaches or scraped one are shared :

#### Marketo Leaked Data
https://t.me/marketo_leaks

#### Breached Data
https://t.me/BreachedData

#### ARVIN
https://t.me/arvin_club

#### DataLeak
https://t.me/freedataleak

####  Leaks Data & Data Base (daily)
https://t.me/LeaksData

#### Lounge
https://t.me/joinchat/_AX7KIImCdpiOTk1

#### Iranian DB
https://t.me/joinchat/xbuTlyrxFIM0NmE0

#### GCBD Big Data Surveillance
https://t.me/gzdata

####  Blank.Room
https://t.me/joinchat/5wChO8HFwJ43YTVk

#### Open Data
https://t.me/opendataleaks

#### Breached DB
https://t.me/BreachedDB

#### H4shur | هاشور
https://t.me/h4shur

#### Data museum
https://t.me/datamuseum

#### DWI DATA LEAKS
https://t.me/DWI_OFFICIAL

#### Database leak
https://t.me/databaseleaked

####  LEAKS AGGREGATOR 
https://t.me/leaks_db

####  Fuckeddb
https://t.me/fuckeddb

## Unsecured buckets

#### Gray Hat Warfare
https://grayhatwarfare.com/
